package com.onlyxiahui.query.hibernate;

/**
 * @author: XiaHui
 */
public class QueryItemException extends Exception {

	private static final long serialVersionUID = 1L;

	public QueryItemException() {
		super();
	}

	public QueryItemException(String message) {
		super(message);
	}
}
