package com.onlyxiahui.query.hibernate;

/**
 * date: 2015年12月31日
 * @author: XiaHui
 */
public enum QueryOptionType {

	likeAll {
		public String getText(Object value) {
			StringBuilder sb = new StringBuilder();
			sb.append("%");
			sb.append(value);
			sb.append("%");
			return sb.toString();
		}
	},
	likeLeft {
		public String getText(Object value) {
			StringBuilder sb = new StringBuilder();
			sb.append("%");
			sb.append(value);
			return sb.toString();
		}
	},
	likeRight {
		public String getText(Object value) {
			StringBuilder sb = new StringBuilder();
			sb.append(value);
			sb.append("%");
			return sb.toString();
		}
	};
	public String getText(Object value) {
		return "";
	}
}
